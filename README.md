# Pintar Pajak

PintarPajak is a platform to learn Indonesia taxes and also simulate the DJP e-filing system.

## Getting Started

First, install all required dependencies:

```bash
sudo npm i -g pnpm 
pnpm install
```

Copy the environment variables file:

```bash
cp .env.example .env.example
```

Migrate the database:

```bash
pnpm prisma-generate
```

And start the development server:

```bash
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### Notes

Always run the migration process after updating `prisma/schema.prisma` file.

```bash
pnpm prisma-generate
```

And if you want to use sqlite as datasource, please go to `prisma/schema.prisma` and change provider to sqlite.