import { FC } from 'react'
import { default as NextHead } from 'next/head'

interface IHeadProps {
  title: string;
  description?: string;
}

const Head: FC<IHeadProps> = ({ title, description }) => (
  <NextHead>
    <title>{title}</title>
  </NextHead>
)

export default Head