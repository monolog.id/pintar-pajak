import { Button } from 'evergreen-ui'
import Link from 'next/link'
import { FC } from 'react'

interface INavBarMenuItemProps {
  text: string;
  href: string;
}

const NavBarMenuItem: FC<INavBarMenuItemProps> = ({ text, href }) => {
  return (
    <Button appearance='minimal' onClick={() => window.location.href = href }>{text}</Button>
  )
}

export default NavBarMenuItem