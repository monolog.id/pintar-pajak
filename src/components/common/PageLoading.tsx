import { Spinner } from 'evergreen-ui'

const PageLoading = () => (
  <div className='w-full h-screen flex justify-center items-center'>
    <div><Spinner /></div>
  </div>
)

export default PageLoading