import axios from 'axios'
import { Dialog, SelectField, TextInputField, toaster } from 'evergreen-ui'
import React, { useState } from 'react'

const DialogLembagaLogin = ({ state, setState }: any) => {
  const [componentState, setComponentState] = useState({
    isLoading: false,
    formData: {
      accountType: '',
      username: '',
      password: ''
    }
  })

  const onChange = (key: string, e: any) => {
    setComponentState({
      ...componentState,
      formData: {
        ...componentState.formData,
        [key]: e.target.value
      }
    })
  }
  
  const onConfirm = async () => {
    const { formData }: any = componentState

    try {
      setComponentState({ ...state, isLoading: true })
      await axios.post('/api/v1/account-institutions/sign-in', { ...formData })
      toaster.success('Sukses', { description: 'Login berhasil. Mohon tunggu ...' })
      setState({ ...state, dialog: {...state.dialog, showSignUp: false, showSignIn: false}})
      setComponentState({ ...state, isLoading: false })
      setTimeout(() => {
        window.location.href = '/lembaga/dashboard'
      }, 2000)
    } catch (e: any) {
      setComponentState({ ...state, isLoading: false })
      const error: string = e.response ? e.response.data.error : 'Terjadi kesalahan. Mohon menghubungi tim support.'
      toaster.danger('Something went wrong', { description: error })
    }
  }

  const forms = [
    {
      id: 'accountType',
      label: 'Tipe Lembaga',
      type: 'select',
      options: [
        { label: 'Universitas', value: 'UNIVERSITAS' },
        { label: 'Institusi', value: 'INSTITUSI' },
        { label: 'Lembaga Pelatihan', value: 'LEMBAGA_PELATIHAN' },
        { label: 'Perusahaan', value: 'PERUSAHAAN' },
      ]
    },
    {
      id: 'username',
      label: 'Alamat E-mail',
      type: 'input',
    },
    {
      id: 'password',
      label: 'Sandi',
      type: 'password',
    }
  ]

  return (
    <React.Fragment>
      <Dialog
        isShown={state.dialog.showSignIn}
        title='Masuk Akun Lembaga'
        onCloseComplete={() => setState({ ...state, dialog: {...state.dialog, showSignIn: false}})}
        confirmLabel='Masuk'
        onConfirm={onConfirm} isConfirmLoading={componentState.isLoading}>
          <div className='space-y-5'>
            {forms.map((form: any) => (
              <div key={form.id}>
                {form.type == 'input' && 
                  <TextInputField label={form.label} onChange={(e: any) => onChange(form.id, e)} />
                }

                {form.type == 'password' &&
                  <div className='ub-mb_24px ub-box-szg_border-box'>
                    <div className='ub-dspl_flex ub-flx-drct_column ub-mb_8px ub-box-szg_border-box'>
                      <label className='ub-color_101840 ub-fnt-fam_b77syt ub-f-wght_500 ub-fnt-sze_14px ub-ln-ht_18px ub-ltr-spc_-0-05px ub-dspl_block ub-mb_0px ub-box-szg_border-box'>{form.label}</label>
                    </div>
                    <input onChange={(e: any) => onChange(form.id, e)} className='css-12rdf6g ub-w_100prcnt ub-fnt-fam_b77syt ub-b-btm_1px-solid-transparent ub-b-lft_1px-solid-transparent ub-b-rgt_1px-solid-transparent ub-b-top_1px-solid-transparent ub-otln_iu2jf4 ub-txt-deco_none ub-bblr_4px ub-bbrr_4px ub-btlr_4px ub-btrr_4px ub-ln-ht_16px ub-fnt-sze_12px ub-color_474d66 ub-pl_12px ub-pr_12px ub-tstn_n1akt6 ub-h_32px ub-bg-clr_white ub-b-btm-clr_d8dae5 ub-b-lft-clr_d8dae5 ub-b-rgt-clr_d8dae5 ub-b-top-clr_d8dae5 ub-f-wght_400 ub-ltr-spc_0 ub-box-szg_border-box' type='password'  aria-invalid='false' id='TextInputField-5' />
                  </div>
                }

                {form.type == 'select' && 
                  <SelectField label={form.label} onChange={(e: any) => onChange(form.id, e)}>
                    <option value='0' selected>Pilih</option>
                    {form.options.map((option: any) => (
                      <option {...option}>{option.label}</option>
                    ))}
                  </SelectField>
                }
              </div>
            ))}
          </div>
      </Dialog>
    </React.Fragment>
  )
}

export default DialogLembagaLogin