import axios from 'axios'
import { Button, Select, Spinner, TextInput, TextInputField } from 'evergreen-ui'
import { stat } from 'fs'
import { FC } from 'react'
import PTKP from 'src/configs/ptkp'
import TarifPPH from 'src/configs/tarif-pph'

const forms: Array<any> = [
  {
    type: 'section',
    label: 'A. Pajak Penghasilan'
  },
  {
    type: 'input',
    id: '1',
    name: 'penghasilanBruto',
    label: 'Penghasilan bruto dalam negeri sehubungan dengan pekerjaan dan penghasilan neto dalam negeri'
  },
  {
    type: 'input',
    id: '2',
    name: 'pengurangan',
    label: 'Pengurangan'
  },
  {
    type: 'penghasilanTidakKenaPajak',
    id: '3',
    name: 'penghasilanTidakKenaPajak',
    label: 'Penghasilan Tidak Kena Pajak'
  },
  {
    type: 'penghasilanKenaPajak',
    id: '4',
    name: 'penghasilanKenaPajak',
    label: 'Penghasilan Kena Pajak'
  },
  {
    type: 'pajakPenghasilanTerutang',
    id: '5',
    name: 'pajakPenghasilanTerutang',
    label: 'Pajak Penghasilan Terutang'
  },
  {
    type: 'input',
    id: '6',
    name: 'pajakPenghasilanDipotongPihakLain',
    label: 'Pajak Penghasilan yang telah dipotong oleh pihak lain'
  },
  {
    type: 'result',
    id: '7',
    name: 'totalLebihKurangBayar',
    label: ''
  },

  {
    type: 'section',
    label: 'B. Penghasilan yang dikenakan PPh Final dan yang dikecualikan dari Objek Pajak'
  },
  {
    type: 'input',
    id: '8',
    name: 'dasarPengenaanPajakPenghasilanBrutoPajakPenghasilanFinal',
    label: 'Dasar Pengenaan Pajak/Penghasilan Bruto Pajak Penghasilan Final'
  },
  {
    type: 'input',
    id: '9',
    name: 'pajakPenghasilanFinalTerutang',
    label: 'Pajak Penghasilan Final Terutang'
  },
  {
    type: 'input',
    id: '10',
    name: 'Penghasilan yang Dikecualikan dari Objek Pajak',
    label: 'Penghasilan yang Dikecualikan dari Objek Pajak'
  },

  {
    type: 'section',
    label: 'C. Daftar Harta dan Kewajiban'
  },
  {
    type: 'input',
    id: '11',
    name: 'jumlahKeseluruhanHartaYangDimilikiPadaAkhirTahunPajak',
    label: 'Jumlah Keseluruhan Harta yang Dimiliki pada Akhir Tahun Pajak'
  },
  {
    type: 'input',
    id: '12',
    name: 'jumlahKeseluruhanKewajibanUtangPadaAkhirTahunPajak',
    label: 'Jumlah Keseluruhan Kewajiban/Utang pada Akhir Tahun Pajak'
  },
]

interface IFormSpt1770SSProps {
  state: any
  setState: Function
  onSubmit?: any
}

const FormSpt1770SS: FC<IFormSpt1770SSProps> = ({ state, setState, onSubmit }) => {
  const roundDown = (value: number) => {
    return 1000 * Math.floor(value / 1000)
  }

  const totalPPh = (harta: any) => {
    var jmlLapis = TarifPPH.length
    let pajak = 0
    var max = 0
    var pengurang, trf
    for (let i = 0; i < jmlLapis; i++) {
      if (i === 0) {
        max = parseFloat(TarifPPH[i][0])
      } else {
        max = parseFloat(TarifPPH[i][0]) - parseFloat(TarifPPH[i - 1][0])
      }
      pengurang = max * 1000000
      trf = parseFloat(TarifPPH[i][1]) / 100
      if (harta > pengurang) {
        pajak = pajak + (pengurang * trf)
        harta = harta - (pengurang)
      } else {
        pajak = pajak + (harta * trf)
        harta = 0
      }
    }
    if (pajak < 0) {
      pajak = 0
    }
    return pajak
  }

  const calculatePPh = (dataSptDetail: any) => {
    let total = roundDown(dataSptDetail.penghasilanBruto - dataSptDetail.pengurangan) - dataSptDetail.penghasilanTidakKenaPajak

    let penghasilanKenaPajak = 0
    let pajakPenghasilanTerutang = 0
    if (total < 0) {
      penghasilanKenaPajak = 0
      pajakPenghasilanTerutang = 0
    } else {
      penghasilanKenaPajak = total
      pajakPenghasilanTerutang = totalPPh(total)
    }

    let totalLebihKurangBayar = pajakPenghasilanTerutang - (dataSptDetail.pajakPenghasilanDipotongPihakLain ? dataSptDetail.pajakPenghasilanDipotongPihakLain : 0)

    let status: string = 'Nihil'
    if (totalLebihKurangBayar > 0) {
      status = 'Kurang Bayar'
    } else if (totalLebihKurangBayar < 0) {
      status = 'Lebih Bayar'
    }
    dataSptDetail = {
      ...dataSptDetail,
      penghasilanKenaPajak,
      pajakPenghasilanTerutang,
      totalLebihKurangBayar,
      status
    }

    setState({
      ...state,
      forms: {
        ...state.forms,
        dataSptDetail
      }
    })
  }

  const onChangeData = (key: string, value: any) => {
    try {
      let dataSptDetail: any = JSON.parse(JSON.stringify(state.forms.dataSptDetail))

      if (key == 'penghasilanTidakKenaPajak') {
        dataSptDetail = {
          ...dataSptDetail,
          penghasilanTidakKenaPajakRaw: value
        }
      }

      try {
        if (key == 'penghasilanTidakKenaPajak') {
          value = JSON.parse(value).value ? JSON.parse(value).value : 0
        } else {
          value = parseInt(value)
        }
      } catch (e) {
        value = 0
      }

      dataSptDetail = {
        ...dataSptDetail,
        [key]: value
      }

      if (key == 'penghasilanBruto' && value > 60000000) {
        return alert('Penghasilan lebih dari Rp 60,000,000 wajib menggunakan Form 1770 S.')
      }

      calculatePPh(dataSptDetail)
    } catch (e) {
      alert('Mohon masukkan data dengan benar.')
    }
  }

  const getValue = (key: string) => {
    return state.forms.dataSptDetail[key] ? state.forms.dataSptDetail[key].toLocaleString() : 0
  }

  if (state.isLoading) {
    return (
      <div className='py-10 space-y-5 flex flex-col justify-center items-center'>
        <Spinner />
        <div>
          Proses menyimpan data ...
        </div>
      </div>
    )
  }

  return (
    <div className='py-10'>
      {forms.map((form: any, i: number) => (
        <div key={i} className='border-l border-r border-b'>
          {form.type == 'section' &&
            <div className='font-bold text-md bg-slate-200 p-2'>{form.label}</div>
          }

          {form.type != 'section' &&
            <div className='grid grid-cols-10 gap-3 p-2'>
              <div className='col-span-1 flex justify-center items-center'>{form.id}</div>
              <div className='col-span-6 flex justify-between items-center'>
                <div>
                  {form.name != 'totalLebihKurangBayar' ? (
                    <>{form.label}</>
                  ):(
                    <span className='font-bold italic'>Status: {state.forms.dataSptDetail.status}</span>
                  )}
                </div>
                {onSubmit && form.type == 'penghasilanTidakKenaPajak' &&
                  <div style={{maxWidth: 300}}>
                    <Select className='w-full' onChange={(e: any) => { onChangeData(form.name, e.target.value) }} >
                      <option value='pilih'>Pilih</option>
                      {PTKP.map((item: any, ptkpid: number) => (
                        <option key={ptkpid} value={JSON.stringify(item)}>{item.label}</option>
                      ))}
                    </Select>
                  </div>
                }

                {!onSubmit && form.type == 'penghasilanTidakKenaPajak' &&
                  <div className='italic font-bold'>{JSON.parse(state.forms.dataSptDetail.penghasilanTidakKenaPajakRaw).label}</div>
                }
              </div>

              <div className='col-span-2'>
                {onSubmit &&
                  <>
                    {form.type == 'input' ? (
                      <TextInput onChange={(e: any) => { onChangeData(form.name, e.target.value) }} />
                    ):(
                      <TextInput disabled value={getValue(form.name)} />
                    )}
                  </>
                }

                <div className={`text-xs mt-2 text-gray-${onSubmit ? '400' : '500 text-right'}`}>
                  Rp. {getValue(form.name)}
                </div>
              </div>
            </div>
          }
        </div>
      ))}

      {onSubmit &&
        <div className='flex justify-center items-center mt-5 space-x-3'>
          {state.profile.userType == 'student' && state.profile.institutionId != '' &&
            <div className='pt-4'>
              <TextInputField placeholder='Masukkan kode simulasi ...'
                onChange={(e: any) => setState({ ...state, simulationCode: e.target.value })}/>
            </div>
          }
          <Button appearance='primary' onClick={() => onSubmit()}>Simpan</Button>
        </div>
      }
    </div>
  )
}

export default FormSpt1770SS