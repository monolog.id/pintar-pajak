import { Button, Label, Select, TextInput } from 'evergreen-ui'
import { FC } from 'react'
import FormTitle from './FormTitle'

interface IFormSptDataFormulirProps {
  state: any;
  setState: Function;
  children?: any;
}

const FormSptDataFormulir: FC<IFormSptDataFormulirProps> = ({ state, setState }) => {
  const onChangeValue = (key: string, value: any) => {
    setState({
      ...state,
      forms: {
        ...state.forms,
        dataFormulir: {
          ...state.forms.dataFormulir,
          [key]: value
        }
      }
    })
  }

  const onClickNext = () => {
    const { dataFormulir }: any = state.forms
    if (!dataFormulir.tahunPajak || (dataFormulir.tahunPajak && dataFormulir.tahunPajak == '0')) return alert('Mohon pilih tahun pajak.')
    if (!dataFormulir.statusSpt || (dataFormulir.statusSpt && dataFormulir.statusSpt == '0')) return alert('Mohon pilih status SPT.')
    if (dataFormulir.statusSpt.toLowerCase() == 'pembetulan') {
      if (!dataFormulir.pembetulanKe) {
        return alert('Mohon diisi form pembetulan ke.')
      }
    }

    setState({ ...state, forms: { ...state.forms, currentIndex: state.forms.currentIndex + 1 }})
  }

  return (
    <div className='mt-5 space-y-5'>
      <FormTitle title='Data Formulir' />

      <div className='grid grid-cols-3 gap-5'>
        <Label>Tahun Pajak</Label>
        <Select onChange={e => { onChangeValue('tahunPajak', e.target.value) }}>
          <option value='0'>Pilih</option>
          <option value='2021'>2021</option>
          <option value='2020'>2020</option>
          <option value='2019'>2019</option>
        </Select>
      </div>

      <div className='grid grid-cols-3 gap-5'>
        <Label>Status SPT</Label>
        <Select onChange={e => { onChangeValue('statusSpt', e.target.value) }}>
          <option value='0'>Pilih</option>
          <option value='normal'>Normal</option>
          <option value='pembetulan'>Pembetulan</option>
        </Select>
      </div>

      {state.forms.dataFormulir.statusSpt == 'pembetulan' &&
        <div className='grid grid-cols-3 gap-5'>
          <Label>Pembetulan ke</Label>
          <TextInput onChange={(e: any) => { onChangeValue('pembetulanKe', e.target.value)}} />
        </div>
      }

      <div className='flex justify-center items-center'>
        <Button appearance='primary' onClick={() => onClickNext()}>Berikutnya</Button>
      </div>
    </div>
  )
}

export default FormSptDataFormulir