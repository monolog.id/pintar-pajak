import { FC } from 'react'

interface IFormTitleProps {
  title: string;
}

const FormTitle: FC<IFormTitleProps> = ({ title }) => (
  <div className='font-bold text-xl'>{title}</div>
)

export default FormTitle