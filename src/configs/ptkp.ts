const PTKP = [
  {
    "label": "TK/0 Tidak Kawin Tidak Ada Tanggungan",
    "value": 54000000
  },
  {
    "label": "TK/1 Tidak Kawin Tanggungan 1 Orang",
    "value": 58500000
  },
  {
    "label": "TK/2 Tidak Kawin Tanggungan 2 Orang",
    "value": 63000000
  },
  {
    "label": "TK/3 Tidak Kawin Tanggungan 3 Orang",
    "value": 67500000
  },
  {
    "label": "K/0 Kawin Tidak Ada Tanggungan",
    "value": 58500000
  },
  {
    "label": "K/1 Kawin Tanggungan 1 Orang",
    "value": 63000000
  },
  {
    "label": "K/2 Kawin Tanggungan 2 Orang",
    "value": 67500000
  },
  {
    "label": "K/3 Kawin Tanggungan 3 Orang",
    "value": 72000000
  },
  {
    "label": "K/I/0 Kawin Penghasilan Istri DIgabung Tidak Ada Tanggungan",
    "value": 58500000
  },
  {
    "label": "K/I/1 Kawin Penghasilan Istri DIgabung Tanggungan 1 Orang",
    "value": 63000000
  },
  {
    "label": "K/I/2 Kawin Penghasilan Istri DIgabung Tanggungan 2 Orang",
    "value": 67500000
  },
  {
    "label": "K/I/3 Kawin Penghasilan Istri DIgabung Tanggungan 3 Orang",
    "value": 72000000
  }
]

export default PTKP