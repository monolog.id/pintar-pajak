import { FC } from 'react'
import Head from 'components/common/Head'
import { Button } from 'evergreen-ui'
import NavBarMenuItem from 'components/common/NavBarMenuItem'
import Link from 'next/link'
import { signIn } from 'next-auth/react'

interface IBaseLayoutProps {
  title: string;
  description?: string;
  children?: any;
  state?: any;
  setState?: Function;
}

const BaseLayout: FC<IBaseLayoutProps> = ({ title, description, children, state, setState }) => {
  const onClickSignIn = () => {
    signIn('google', { callbackUrl: `${window.location.origin}/dashboard` })
  }

  return (
    <div>
      <Head title={title} description={description} />

      <div className='p-3 grid grid-cols-1 md:grid-cols-3 border-b'>
        <div className='text-center flex items-center md:text-left'>
          <Link href='/'>
            <span className='text-primary-color tracking-wider'>
              PINTAR
              <span className='font-bold'>PAJAK</span>
            </span>
          </Link>
        </div>
        <div className='flex justify-center items-center'>
          <NavBarMenuItem text='Tentang Kami' href='/tentang-kami' />
          <NavBarMenuItem text='Pelatihan Pajak' href='/brevet-pajak' />
          <NavBarMenuItem text='Untuk Lembaga dan Universitas' href='/lembaga' />
          {/* <NavBarMenuItem text='Simulasi SPT' href='/simulasi-spt' />
          <NavBarMenuItem text='Harga' href='/harga' />
          <NavBarMenuItem text='FAQ' href='/faq' /> */}
        </div>
        <div className='flex justify-center md:justify-end items-center space-x-3'>
          <Button appearance='minimal' onClick={onClickSignIn}>Masuk menggunakan Google</Button>
          <Button appearance='primary' onClick={onClickSignIn}>Coba Gratis</Button>
        </div>
      </div>

      {children}
    </div>
  )
}

export default BaseLayout