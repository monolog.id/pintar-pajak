import { FC } from 'react'
import Head from 'components/common/Head'
import { Avatar, Button, Menu, Popover, Position } from 'evergreen-ui'
import NavBarMenuItem from 'components/common/NavBarMenuItem'
import Link from 'next/link'
import { signIn, signOut } from 'next-auth/react'

interface IDashboardLayoutProps {
  title: string;
  description?: string;
  children?: any;
  state?: any;
  setState?: Function;
}

const DashboardLayout: FC<IDashboardLayoutProps> = ({ title, description, children, state, setState }) => {
  return (
    <div>
      <Head title={title} description={description} />

      <div className='p-3 flex flex-col md:flex-row  border-b justify-between items-center'>
        <div className='flex items-center space-x-10'>
          <Link href='/'> 
            <span className='text-primary-color tracking-wider'>
              PINTAR
              <span className='font-bold'>PAJAK</span>
            </span>
          </Link>
          <div>
            <NavBarMenuItem text='Dashboard' href='/dashboard' />
            <NavBarMenuItem text='Simulasi' href='/simulasi' />
          </div>
        </div>

        <div className='flex items-center space-x-5'>
          <Avatar
            src={state.user.image}
            name={state.user.name}
            size={26}
          />
          <Popover
            position={Position.BOTTOM_LEFT}
            content={
              <Menu>
                <Menu.Group>
                  <Menu.Item>Profile</Menu.Item>
                  <Menu.Item>Setting</Menu.Item>
                </Menu.Group>
                <Menu.Divider />
                <Menu.Group>
                  <Menu.Item onSelect={() => signOut({callbackUrl:`${window.location.origin}/auth-logout`})} intent="danger">
                    Logout
                  </Menu.Item>
                </Menu.Group>
              </Menu>
            }
          >
            <Button appearance="minimal" marginRight={16}>{state.user.name}</Button>
          </Popover>
        </div>
      </div>
      
      <div>
        {children}
      </div>
    </div>
  )
}

export default DashboardLayout