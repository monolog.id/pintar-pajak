import { FC } from 'react'
import Head from 'components/common/Head'
import { Button } from 'evergreen-ui'
import NavBarMenuItem from 'components/common/NavBarMenuItem'
import Link from 'next/link'

interface ILembagaBaseLayoutProps {
  title: string;
  description?: string;
  children?: any;
  state: any;
  setState: Function;
}

const LembagaBaseLayout: FC<ILembagaBaseLayoutProps> = ({ title, description, children, state, setState }) => {
  return (
    <div>
      <Head title={title} description={description} />

      <div className='p-3 grid grid-cols-2 md:grid-cols-3 border-b'>
        <div className='text-center flex items-center md:text-left'>
          <Link href='/'>
            <span className='cursor-pointer text-primary-color tracking-wider'>
              PINTAR
              <span className='font-bold'>PAJAK</span>
            </span>
          </Link>
        </div>
        <div className='flex justify-center items-center'>
          <NavBarMenuItem text='Tentang Kami' href='/tentang-kami' />
          <NavBarMenuItem text='Pelatihan Pajak' href='/brevet-pajak' />
          <NavBarMenuItem text='Untuk Lembaga dan Universitas' href='/lembaga' />
        </div>
        <div className='flex justify-center md:justify-end items-center space-x-3'>
          <Button appearance='minimal' onClick={() => setState({ ...state, dialog: {...state.dialog, showSignIn: true}})}>Masuk</Button>
          <Button appearance='primary' onClick={() => setState({ ...state, dialog: {...state.dialog, showSignUp: true}})}>Daftar</Button>
        </div>
      </div>

      {children}
    </div>
  )
}

export default LembagaBaseLayout