import { FC } from 'react'
import Head from 'components/common/Head'
import { Button } from 'evergreen-ui'
import NavBarMenuItem from 'components/common/NavBarMenuItem'
import Link from 'next/link'

interface ILembagaDashboardLayoutProps {
  title: string;
  description?: string;
  children?: any;
  state: any;
  setState: Function;
}

const LembagaDashboardLayout: FC<ILembagaDashboardLayoutProps> = ({ title, description, children, state, setState }) => {
  return (
    <div>
      <Head title={title} description={description} />

      <div className='p-3 border-b'>
        <div className='container mx-auto flex justify-between items-center'>
          <div className='text-center flex items-center md:text-left'>
            <Link href='/'>
              <span className='cursor-pointer text-primary-color tracking-wider'>
                PINTAR
                <span className='font-bold'>PAJAK</span>
              </span>
            </Link>
          </div>
          <div className='flex justify-end items-center space-x-3'>
            <Button appearance='minimal' onClick={() => window.location.href = '/lembaga/dashboard'}>Dashboard</Button>
            <Button appearance='minimal' onClick={() => window.location.href = '/lembaga/logout'}>Log Out</Button>
          </div>
        </div>
      </div>

      {children}
    </div>
  )
}

export default LembagaDashboardLayout