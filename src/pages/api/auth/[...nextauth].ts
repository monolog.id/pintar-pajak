import NextAuth, { NextAuthOptions } from 'next-auth'
import GoogleProvider from 'next-auth/providers/google'
import { PrismaAdapter } from '@next-auth/prisma-adapter'
import prismadb from 'lib/prismadb'
import prisma from 'lib/prismadb'

export const authOptions: NextAuthOptions = {
  adapter: PrismaAdapter(prismadb),
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_ID || '',
      clientSecret: process.env.GOOGLE_SECRET || '',
    }),
  ],
  session:{
    strategy:'jwt',
    maxAge: 7 * 24 * 60 * 60, // 7 days
  },
  events: {
    createUser: async ({ user }: any) => {
        await prisma.user.update({
          where: { id: user.id },
          data: { emailVerified: new Date() },
        })
    },
    // updateUser({ user })
  },
}

export default NextAuth(authOptions)