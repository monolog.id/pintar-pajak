import { NextApiRequest, NextApiResponse } from 'next'
import { ApiResponseJson } from 'src/utils/api-response'
import bcrypt from 'bcrypt'
import prisma from 'lib/prismadb'

export default async function apiV1AccountInstitutionsRegister(req: NextApiRequest, res: NextApiResponse) {
  const { accountType, username, password, fullname }: any = req.body

  if (['UNIVERSITAS','INSTITUSI','LEMBAGA_PELATIHAN','PERUSAHAAN'].indexOf(accountType) < 0) {
    return ApiResponseJson(res, { error: 'Invalid type.' }, 400)
  }

  if (!username.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/gi)) {
    return ApiResponseJson(res, { error: 'Invalid username. Please use a valid email address' }, 400)
  }

  if (!password) return ApiResponseJson(res, { error: 'Invalid password.' }, 400)
  if (password && password.length < 8) return ApiResponseJson(res, { error: 'Min. password length should be more than 8 characters.' }, 400)

  if (!fullname) return ApiResponseJson(res, { error: 'Invalid fullname.' }, 400)
  if (fullname && fullname.length < 3) return ApiResponseJson(res, { error: 'Min. fullname length should be more than 3 characters.' }, 400)

  try {
    bcrypt.hash(password, 10, async (err: any, hashPassword: string) => {
      if (err) return ApiResponseJson(res, { error: 'Faild to encrypt password.' }, 500)
  
      await prisma.accountInstitution.create({
        data: {
          accountType,
          username,
          password: hashPassword,
          fullname
        }
      })

      ApiResponseJson(res, {}, 201)
    })
  } catch (e: any) {
    ApiResponseJson(res, { error: 'Something went wrong. Failed to create new account.' }, 500)
  }
}