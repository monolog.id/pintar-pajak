import bcrypt from 'bcrypt'
import prisma from 'lib/prismadb'
import { NextApiRequest, NextApiResponse } from 'next'
import nookies from 'nookies'
import { ApiResponseJson } from 'src/utils/api-response'
import JwtUtil from 'src/utils/jwt'

export default async function apiV1AccountInstitutionsSignIn(req: NextApiRequest, res: NextApiResponse) {
  const { accountType, username, password }: any = req.body

  if (!username || username && !username.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/gi)) {
    return ApiResponseJson(res, { error: 'Invalid username. Please use a valid email address' }, 400)
  }

  if (!password) return ApiResponseJson(res, { error: 'Invalid password.' }, 400)
  if (password && password.length < 8) return ApiResponseJson(res, { error: 'Min. password length should be more than 8 characters.' }, 400)

  try {
    let account: any = await prisma.accountInstitution.findFirst({ where: { accountType, username }})
    if (!account) return ApiResponseJson(res, { error: 'Invalid username. Account did not exist.' }, 500)

    bcrypt.compare(password, account.password, (err: any, result: boolean) => {
      if (err) return ApiResponseJson(res, { error: 'Something went wrong. Failed to login due to password validation.' }, 500)
      if (!result) return ApiResponseJson(res, { error: 'Wrong password.' }, 500)

      delete account.password
      delete account.createdAt
      delete account.updatedAt
      delete account.deletedAt

      const token = new JwtUtil().generateToken(account)

      nookies.set({ res }, 'pintarpajak.session', token, {
        maxAge: 30 * 24 * 60 * 60,
        path: '/',
      });

      ApiResponseJson(res, { account }, 200)
    })
  } catch (e: any) {
    console.dir(e)
    return ApiResponseJson(res, { error: 'Something went wrong. Failed to login.' }, 500)
  }
}