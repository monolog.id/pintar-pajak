import { NextApiRequest, NextApiResponse } from 'next'
import { ApiResponseJson } from 'src/utils/api-response'
import prisma from 'lib/prismadb'

export default async function apiV1InstitutionSimulationCodeGetAll(req: NextApiRequest, res: NextApiResponse) {
  try {
    let result: any = {}
    if (req.query.code && req.query.institutionId ) {
      result = await prisma.institutionSimulationCode.findFirst({ where: { code: req.query.code!.toString(), institutionId: req.query.institutionId!.toString() }})
    } else {
      result = await prisma.institutionSimulationCode.findMany({ orderBy: { createdAt: 'asc' }})
    }

    ApiResponseJson(res, { institutionSimulationCodes: result }, 201)
  } catch (e: any) {
    ApiResponseJson(res, { error: 'Something went wrong. Failed to get insitution simulation code.' }, 500)
  }
}