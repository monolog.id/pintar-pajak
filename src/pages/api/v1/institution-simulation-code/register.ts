import { NextApiRequest, NextApiResponse } from 'next'
import { ApiResponseJson } from 'src/utils/api-response'
import prisma from 'lib/prismadb'

export default async function apiV1InstitutionSimulationCodeRegister(req: NextApiRequest, res: NextApiResponse) {
  const { institutionId, accountInstitutionId, code, schedule, simulationType }: any = req.body

  if (!institutionId) return ApiResponseJson(res, { error: 'Invalid institution id.' }, 400)
  if (!accountInstitutionId) return ApiResponseJson(res, { error: 'Invalid account institution id.' }, 400)
  if (!code) return ApiResponseJson(res, { error: 'Invalid code.' }, 400)
  if (code && code.length < 5) return ApiResponseJson(res, { error: 'Min. code length should be more than 5 characters.' }, 400)

  try {
    let result: any = await prisma.institutionSimulationCode.findFirst({ where: { institutionId, code } })
    if (result) return ApiResponseJson(res, { error: 'Kode simulasi sudah terdaftar sebelumnya. Silahkan pilih kode simulasi yang baru.' }, 400)

    result = await prisma.institutionSimulationCode.create({ data: req.body })

    ApiResponseJson(res, { institutionSimulationCode: result }, 201)
  } catch (e: any) {
    ApiResponseJson(res, { error: 'Something went wrong. Failed to create new simulation code.' }, 500)
  }
}