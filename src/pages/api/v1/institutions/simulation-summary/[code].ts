import prisma from 'lib/prismadb'
import { NextApiRequest, NextApiResponse } from "next";
import { ApiResponseJson } from "src/utils/api-response";

export default async function apiV1InstitutionsSimulationSummaryByCode(req: NextApiRequest, res: NextApiResponse) {
  const { code }: any = req.query
  try {
    let result: any = await prisma.sptSimulation.findMany({
      where: { simulationCode: code },
      orderBy: { createdAt: 'asc' },
      include: {
        user: true
      }
    })

    ApiResponseJson(res, { simulationSummary: result }, 200)
  } catch (e: any) {
    ApiResponseJson(res, { error: 'Gagal mengambil data rangkuman simulasi berdasarkan code.' }, 500)
  }
}