import prisma from 'lib/prismadb'
import { NextApiRequest, NextApiResponse } from 'next'
import { getToken } from 'next-auth/jwt'

export default async function apiV1SimulationSptDetail(req: NextApiRequest, res: NextApiResponse) {
  const profile: any = getToken({ req })
  if (!profile) return res.status(401).json({ error: 'Akun ini tidak memiliki akses.' })

  const result: any = await prisma.sptSimulation.findFirst({ where: { id: req.query.id?.toString() }, include: { user: true }})
  if (!result) return res.status(404).json({ error: 'Data simulasi SPT tidak ditemukan.' })

  res.status(200).json({ spt: result })
}