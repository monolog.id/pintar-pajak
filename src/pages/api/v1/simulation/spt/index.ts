import prisma from 'lib/prismadb'
import { NextApiRequest, NextApiResponse } from 'next'
import { getToken } from 'next-auth/jwt'

export default async function apiV1SimulationSpt(req: NextApiRequest, res: NextApiResponse) {
  const profile: any = await getToken({ req })
  if (!profile) return res.status(401).json({ error: 'Akun ini tidak memiliki akses.' })

  const { method }: any = req

  let result: any = {}
  switch (method) {
    // Save spt simulation data
    case 'POST':
      result = await createSptSimulation(profile, req.body)
      break
    default:
      result = await getAllSptSimulation(profile)
  }

  if (result.error) return res.status(500).json({ error: result.error })
  res.status(200).json({ ...result })
}

const getAllSptSimulation = async (profile: any) => {
  const result: any = await prisma.sptSimulation.findMany({ where: { userId: profile.sub }})
  return { sptSimulations: result }
}

const createSptSimulation = async (profile: any, params: any) => {
  if (!params.dataFormulir.tahunPajak || (params.dataFormulir && params.dataFormulir.tahunPajak == '0')) return { error: 'Mohon pilih tahun pajak.' }
  if (!params.dataFormulir.statusSpt || (params.dataFormulir && params.dataFormulir.statusSpt == '0')) return { error: 'Mohon pilih status pajak.' }

  // const user: any = params.user
  // const userData: any = await prisma.user.findFirst({ where: { email: user.email }})
  // if (!userData) return { error: 'Akun tidak memiliki akses untuk menyimpan simulasi data SPT.' }

  let data: any = {
    userId: profile.sub,
    simulationCode: params.simulationCode ? params.simulationCode : '',
    sptType: params.sptType,
    year: params.dataFormulir.tahunPajak,
    submissionType: params.dataFormulir.statusSpt,
    submissionPeriod: params.dataFormulir.pembetulanKe ? params.dataFormulir.pembetulanKe : '0',
    formSpt: JSON.stringify({
      dataFormulir: params.dataFormulir,
      dataSptDetail: params.dataSptDetail
    })
  }

  try {
    const result: any = await prisma.sptSimulation.create({ data })
    return { sptSimulation: result }
  } catch (e) {
    console.dir(e)
    return { error: 'Terjadi kesalahan saat proses penyimpanan data simulasi spt.' }
  }
}