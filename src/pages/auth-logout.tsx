import { Spinner } from 'evergreen-ui'
import { NextPageContext } from 'next'
import nookies from 'nookies'

export async function getServerSideProps(ctx: NextPageContext) {
  nookies.destroy(ctx, 'pintarpajak.session.profile', {
    path: '/',
  })

  return {
    redirect: {
      destination: '/',
      permanent: false
    }
  }
}

const AuthLogout = () => {
  return (
    <div className='w-full h-screen flex justify-center items-center'>
      <Spinner />
    </div>
  )
}

export default AuthLogout