import { Avatar, Button, InboxIcon } from 'evergreen-ui'
import { useState } from 'react'
import DashboardLayout from 'layouts/DashboardLayout'
import { dashboardGetServerSideProps } from 'src/utils/server-side-props'

export default function Dashboard({ user }: any) {
  const [state, setState] = useState<any>({ user })

  return (
    <DashboardLayout title='Dashboard' state={state} setState={setState}>
      <div className='p-10 grid grid-cols-1 md:grid-cols-3 gap-3'>
        <div className="md:col-span-2">
          <div className='border rounded-md p-5 flex items-center justify-between'>
            <div className='flex'>
              <Avatar
                src={user.image}
                name={user.name}
                size={48}
              />
              <div className='ml-3'>
                <div>{user.name}</div>
                <div>{user.email}</div>
              </div>
            </div>
            <div>
            <Button appearance="primary">
                Primary
              </Button>
            </div>
          </div>
        </div>

        <div>
          <div className='border rounded-md p-5'>
            Personal Informations
            <div className='flex items-center'>
            <InboxIcon marginRight={10}/>
              {user.email}
            </div>
          </div>
        </div>
      </div>
    </DashboardLayout>
  )
}

export const getServerSideProps = dashboardGetServerSideProps