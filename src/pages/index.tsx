import SvgJoin from 'components/svg/SvgJoin'
import { Button } from 'evergreen-ui'
import BaseLayout from 'layouts/BaseLayout'
import type { NextPage } from 'next'
import { signIn } from 'next-auth/react'

const Home: NextPage = () => {
  const onClickSignIn = () => {
    signIn('google', { callbackUrl: `${window.location.origin}/dashboard` })
  }

  return (
    <BaseLayout title={'Pintar Pajak'}>
      <div className='w-full p-10 md:p-20 flex justify-center items-center border-b'>
        <div style={{maxWidth: '600px' }}>
          <div className='text-primary-color text-center font-bold text-4xl'>
            Satu-satunya Platform Belajar Pajak Yang Sangat Mudah Digunakan
          </div>
          <div className='text-center py-10'>
            Pintar Pajak adalah sebuah platform untuk belajar brevet pajak
            dan simulasi pengisian SPT untuk memudahkan pengguna
            dalam menggunakan sistem DJP.
          </div>
          <div className='text-center'>
          <Button appearance='primary' size='large' onClick={onClickSignIn}>Coba Gratis Sekarang Juga!</Button>
          </div>
        </div>
      </div>

      <div className='w-full p-10 md:p-20 flex flex-col md:flex-row justify-between items-center gap-10'>
        <div className='space-y-5'>
          <div className='font-bold text-4xl text-primary-color'>
            Tingkatkan Skill Untuk Karir Profesional!
          </div>
          <div style={{maxWidth: 600}}>
            Platform Pintar Pajak sudah dipercaya lebih dari 10.000 orang untuk
            mengembangkan skill tentang pajak di Indonesia.
          </div>
          <div>
          <Button appearance='primary' onClick={onClickSignIn}>Yuk, coba GRATIS sekarang!</Button>
          </div>
        </div>
        <div className='flex justify-center'>
          <SvgJoin width={300} />
        </div>
      </div>
    </BaseLayout>
  )
}

export default Home
