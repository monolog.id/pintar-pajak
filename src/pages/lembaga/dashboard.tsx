import LembagaDashboardLayout from 'layouts/LembagaDashboardLayout'
import type { NextPage, NextPageContext } from 'next'
import { useEffect, useState } from 'react'
import prisma from 'lib/prismadb'
import { AddIcon, Button, EyeOpenIcon, IconButton, Spinner, Table, toaster, TrashIcon } from 'evergreen-ui'
import verifyProfile from 'src/utils/verify-profile'
import DialogLembagaCreateSimulationCode from 'components/dialog/DialogLembagaCreateSimulationCode'
import axios from 'axios'

export async function getServerSideProps(ctx: NextPageContext) {
  const profile: any = verifyProfile(ctx)
  if (profile.error) return { redirect: profile.redirect }

  let institution: any = await prisma.institution.findFirst({ where: { accountInstitutionId: profile.id } })
  if (institution) institution = JSON.stringify(institution)

  return {
    props: {
      profile,
      institution
    }
  }
}

const Dashboard: NextPage = ({ profile, institution: institutionData }: any) => {
  const institution: any = institutionData ? JSON.parse(institutionData) : {}
  const [state, setState] = useState<any>({
    isLoading: true,
    dialog: {
      showCreateSimulationCode: false
    },
    profile,
    institution,
    institutionSimulationCodes: []
  })

  const setDialog = (key: string, value: boolean) => {
    setState({
      ...state,
      dialog: {
        ...state.dialog,
        [key]: value
      }
    })
  }

  useEffect(() => {
    const onLoad = async () => {
      try {
        let result: any = await axios.get('/api/v1/institution-simulation-code')
        const { institutionSimulationCodes }: any = result.data
        setState({
          ...state,
          isLoading: false,
          institutionSimulationCodes
        })
      } catch (e: any) {
        const description: string = e.response ? e.response.data.error : 'Gagal mengambil data.'
        toaster.danger('Terjadi kesalahan', { description })
        setState({ ...state, isLoading: false })
      }
    }

    onLoad()
  }, [])

  return (
    <LembagaDashboardLayout title={'Pintar Pajak'} state={state} setState={setState}>
      <div className='container mx-auto grid grid-cols-1 md:grid-cols-4 gap-5 p-5 md:p-0 mt-5'>
        <div className='md:col-span-1'>
          <div className='uppercase text-sm tracking-wide mb-5'>
            Selamat datang<br/>
            {institution &&
              <span className='font-bold '>
                {institution.institutionType} {institution.name}
              </span>
            }
          </div>
          <div className='text-2xl uppercase font-bold'>{profile.fullname}</div>
          <div className='space-x-2 mt-5'></div>
        </div>
        <div className='md:col-span-3'>
          <div className='flex justify-between items-center mb-5'>
            <div className='font-bold'>Rangkuman Simulasi</div>
            <div className='font-bold'>
              <Button appearance='primary' iconBefore={AddIcon} onClick={() => setDialog('showCreateSimulationCode', true)}>Buat Kode Simulasi</Button>
            </div>
          </div>
          
          {state.isLoading && <Spinner />}
          {!state.isLoading &&
            <Table>
              <Table.Head>
                <Table.TextHeaderCell>Jadwal</Table.TextHeaderCell>
                <Table.TextHeaderCell>Kode Simulasi</Table.TextHeaderCell>
                <Table.TextHeaderCell>Tipe Simulasi</Table.TextHeaderCell>
                <Table.TextHeaderCell></Table.TextHeaderCell>
              </Table.Head>
              <Table.Body>
                {state.institutionSimulationCodes && state.institutionSimulationCodes.map((item: any) => (
                  <Table.Row key={item.id}>
                    <Table.TextCell>{item.schedule}</Table.TextCell>
                    <Table.TextCell>{item.code}</Table.TextCell>
                    <Table.TextCell>{item.simulationType}</Table.TextCell>
                    <Table.Cell>
                      <div className='space-x-3'>
                        <IconButton icon={EyeOpenIcon} onClick={() => window.location.href = `/lembaga/rangkuman-simulasi/${item.code}`} />
                        <IconButton icon={TrashIcon} intent='danger' disabled />
                      </div>
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
          }
        </div>
      </div>

      <DialogLembagaCreateSimulationCode
        state={state}
        setState={setState}
        setDialog={setDialog} />
    </LembagaDashboardLayout>
  )
}

export default Dashboard
