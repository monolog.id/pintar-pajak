import DialogLembagaLogin from 'components/dialog/DialogLembagaLogin'
import DialogLembagaSignUp from 'components/dialog/DialogLembagaSignUp'
import { Button } from 'evergreen-ui'
import LembagaBaseLayout from 'layouts/LembagaBaseLayout'
import type { NextPage } from 'next'
import { useState } from 'react'

const Home: NextPage = () => {
  const [state, setState] = useState<any>({
    dialog: {
      showSignUp: false,
      showSignIn: false
    }
  })

  return (
    <LembagaBaseLayout title={'Pintar Pajak'} state={state} setState={setState}>
      <div className='w-full p-10 md:p-20 flex justify-center items-center border-b'>
        <div style={{maxWidth: '600px' }}>
          <div className='text-primary-color text-center font-bold text-4xl'>
            Sekarang Anda bisa punya akun untuk lembaga Anda sendiri
          </div>
          <div className='text-center py-10'>
            Buat akun lembaga untuk bisa mengulas hasil simulasi dari mahasiswa/i maupun peserta training dari lembaga Anda.
          </div>
          <div className='flex justify-center items-center text-center'>
            <div className='space-x-3'>
              <Button appearance='minimal' size='large' onClick={() => setState({ ...state, dialog: {...state.dialog, showSignIn: true}})}>Masuk</Button>
              <Button appearance='primary' size='large' onClick={() => setState({ ...state, dialog: {...state.dialog, showSignUp: true}})}>Daftar</Button>
            </div>
          </div>
        </div>
      </div>

      <DialogLembagaSignUp state={state} setState={setState} />
      <DialogLembagaLogin state={state} setState={setState} />
    </LembagaBaseLayout>
  )
}

export default Home
