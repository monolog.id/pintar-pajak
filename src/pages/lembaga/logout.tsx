import { NextPageContext } from 'next'
import { destroyCookie } from 'nookies'

export async function getServerSideProps(ctx: NextPageContext) {
  await destroyCookie(ctx, 'pintarpajak.session', { path: '/' })
  return {
    redirect: {
      destination: '/',
      permanent: false
    }
  }
}

export default function page() {
  return (
    <></>
  )
}