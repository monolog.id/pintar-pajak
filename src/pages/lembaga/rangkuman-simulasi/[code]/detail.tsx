import axios from 'axios'
import PageLoading from 'components/common/PageLoading'
import FormSpt1770SS from 'components/form/FormSpt1770SS'
import { Tab, Table } from 'evergreen-ui'
import DashboardLayout from 'layouts/DashboardLayout'
import LembagaDashboardLayout from 'layouts/LembagaDashboardLayout'
import prisma from 'lib/prismadb'
import moment from 'moment'
import { NextPage, NextPageContext } from 'next'
import { useEffect, useState } from 'react'
import { dashboardGetServerSideProps } from 'src/utils/server-side-props'
import verifyProfile from 'src/utils/verify-profile'

export async function getServerSideProps(ctx: NextPageContext) {
  const { query }: any = ctx
  const profile: any = verifyProfile(ctx)
  if (profile.error) return { redirect: profile.redirect }

  let institution: any = await prisma.institution.findFirst({ where: { accountInstitutionId: profile.id } })
  if (institution) institution = JSON.stringify(institution)

  return {
    props: {
      profile,
      institution,
      query
    }
  }
}

const SptDetail: NextPage = ({ query }: any) => {
  const [state, setState] = useState<any>({
    isLoading: true,
    forms: {
      dataFormulir: {},
      dataSptDetail: {}
    },
    spt: {},
    user: {}
  })

  const getSptSimulation = async (id: string) => {
    try {
      const result: any = await axios.get(`/api/v1/simulation/spt/${id}`)

      let spt: any = result.data.spt
      spt.formSpt = JSON.parse(spt.formSpt)

      setState({
        ...state, isLoading: false,
        spt,
        user: spt.user || {},
        forms: {
          dataFormulir: spt.formSpt.dataFormulir,
          dataSptDetail: spt.formSpt.dataSptDetail
        }
      })
    } catch (e: any) {
      setState({ ...state, isLoading: false })
      console.dir(e)
      alert('Terjadi kesalahan.')
    }
  }

  useEffect(() => {
    getSptSimulation(query.id)
  }, [])

  if (state.isLoading) {
    return <PageLoading />
  }

  return (
    <LembagaDashboardLayout title='Pintar Pajak' state={state} setState={setState}>
      <div className='container mx-auto mt-5'>
        { state.user &&
          <Table>
            <Table.Head>
              <Table.HeaderCell>Nama Peserta</Table.HeaderCell>
              <Table.HeaderCell>Email</Table.HeaderCell>
              <Table.HeaderCell>Kode Simulasi</Table.HeaderCell>
              <Table.HeaderCell>Tanggal Kirim</Table.HeaderCell>
            </Table.Head>
            <Table.Body>
              <Table.Row>
                <Table.TextCell>{state.user.name}</Table.TextCell>
                <Table.TextCell>{state.user.email}</Table.TextCell>
                <Table.TextCell>{state.spt.simulationCode}</Table.TextCell>
                <Table.TextCell>{moment(state.spt.createdAt).format('YYYY-MM-DD HH:mm:ss')}</Table.TextCell>
              </Table.Row>
            </Table.Body>
          </Table>
        }

        <FormSpt1770SS state={state} setState={setState} />
      </div>
    </LembagaDashboardLayout>
  )
}

export default SptDetail