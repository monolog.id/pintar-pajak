import LembagaDashboardLayout from 'layouts/LembagaDashboardLayout'
import type { NextPage, NextPageContext } from 'next'
import { useEffect, useState } from 'react'
import prisma from 'lib/prismadb'
import { AddIcon, Button, EyeOpenIcon, IconButton, Spinner, Tab, Table, TableBody, toaster, TrashIcon } from 'evergreen-ui'
import verifyProfile from 'src/utils/verify-profile'
import DialogLembagaCreateSimulationCode from 'components/dialog/DialogLembagaCreateSimulationCode'
import axios from 'axios'
import moment from 'moment'

export async function getServerSideProps(ctx: NextPageContext) {
  const { query }: any = ctx
  const profile: any = verifyProfile(ctx)
  if (profile.error) return { redirect: profile.redirect }

  let institution: any = await prisma.institution.findFirst({ where: { accountInstitutionId: profile.id } })
  if (institution) institution = JSON.stringify(institution)

  return {
    props: {
      profile,
      institution,
      code: query.code
    }
  }
}

const RangkumanSimulasi: NextPage = ({ profile, institution: institutionData, code }: any) => {
  const institution: any = institutionData ? JSON.parse(institutionData) : {}
  const [state, setState] = useState<any>({
    isLoading: true,
    dialog: {
      showCreateSimulationCode: false
    },
    profile,
    institution,
    institutionSimulationCodes: {}
  })

  const setDialog = (key: string, value: boolean) => {
    setState({
      ...state,
      dialog: {
        ...state.dialog,
        [key]: value
      }
    })
  }

  useEffect(() => {
    const onLoad = async () => {
      try {
        let result: any = await axios.get(`/api/v1/institution-simulation-code?code=${code}&institutionId=${institution.id}`)
        const { institutionSimulationCodes }: any = result.data

        result = await axios.get(`/api/v1/institutions/simulation-summary/${code}`)
        const { simulationSummary }: any = result.data
        console.dir(simulationSummary)
        setState({
          ...state,
          isLoading: false,
          institutionSimulationCodes,
          simulationSummary
        })
      } catch (e: any) {
        const description: string = e.response ? e.response.data.error : 'Gagal mengambil data.'
        toaster.danger('Terjadi kesalahan', { description })
        setState({ ...state, isLoading: false })
      }
    }

    onLoad()
  }, [])

  return (
    <LembagaDashboardLayout title={'Pintar Pajak'} state={state} setState={setState}>
      <div className='container mx-auto grid grid-cols-1 md:grid-cols-4 gap-5 p-5 md:p-0 mt-5'>
        <div className='md:col-span-1'>
          <div className='space-y-5'>
            <div className='space-y-1'>
              <div className='uppercase text-xs text-gray-500'>Lembaga</div>
              <div className='uppercase text-md font-bold'>
                {institution.institutionType} {institution.name}
              </div>
            </div>
            <div className='space-y-1'>
              <div className='uppercase text-xs text-gray-500'>Jadwal</div>
              <div className='uppercase text-md font-bold'>
                {state.institutionSimulationCodes.schedule}
              </div>
            </div>
            <div className='space-y-1'>
              <div className='uppercase text-xs text-gray-500'>Kode Simulasi</div>
              <div className='uppercase text-md font-bold'>
                {state.institutionSimulationCodes.code}
              </div>
            </div>
            <div className='space-y-1'>
              <div className='uppercase text-xs text-gray-500'>Tipe Simulasi</div>
              <div className='uppercase text-md font-bold'>
                {state.institutionSimulationCodes.simulationType}
              </div>
            </div>
          </div>
        </div>
        <div className='md:col-span-3'>
          <div className='flex justify-between items-center mb-5'>
            <div className='font-bold'>Rangkuman Simulasi Peserta</div>
          </div>
          
          {state.isLoading && <Spinner />}
          {!state.isLoading &&
            <Table>
              <Table.Head>
                <Table.HeaderCell>Peserta</Table.HeaderCell>
                <Table.HeaderCell>Tipe</Table.HeaderCell>
                <Table.HeaderCell>Tahun</Table.HeaderCell>
                <Table.HeaderCell>Tanggal Kirim</Table.HeaderCell>
                <Table.HeaderCell></Table.HeaderCell>
              </Table.Head>
              <TableBody>
                {state.simulationSummary.map((item: any) => (
                  <Table.Row key={item.id}>
                    <Table.TextCell>
                      <div>{item.user.name}</div>
                      <div className='text-xs text-gray-500'>{item.user.email}</div>
                    </Table.TextCell>
                    <Table.TextCell>{item.sptType}</Table.TextCell>
                    <Table.TextCell>{item.year}</Table.TextCell>
                    <Table.TextCell>{moment(item.createdAt).format('YYYY-MM-DD HH:mm:ss')}</Table.TextCell>
                    <Table.TextCell>
                      <div className='space-x-3'>
                        <IconButton icon={EyeOpenIcon} onClick={() => window.open(`/lembaga/rangkuman-simulasi/${code}/detail?id=${item.id}`, '_tab')} />
                      </div>
                    </Table.TextCell>
                  </Table.Row>
                ))}
              </TableBody>
            </Table>
          }
        </div>
      </div>
    </LembagaDashboardLayout>
  )
}

export default RangkumanSimulasi
