import axios from 'axios'
import { Button, DocumentOpenIcon, IconButton, Spinner, Table } from 'evergreen-ui'
import DashboardLayout from 'layouts/DashboardLayout'
import moment from 'moment'
import { NextPage } from 'next'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import { dashboardGetServerSideProps } from 'src/utils/server-side-props'

export const getServerSideProps = dashboardGetServerSideProps

const Simulasi: NextPage = ({ user }: any) => {
  const [state, setState] = useState<any>({
    user,
    isLoading: true,
    sptSimulations: []
  })

  const getAllSptSimulations = async () => {
    try {
      const result = await axios.get('/api/v1/simulation/spt')
      setState({ ...state, isLoading: false, sptSimlations: result.data.sptSimulations || [] })
    } catch (e) {
      console.dir(e)
      alert('Terjadi kesalahan pada saat proses pengambilan data simulasi.')
    }
  }

  useEffect(() => {
    getAllSptSimulations()
  }, [])

  return (
    <DashboardLayout title='Simulasi Pajak' state={state} setState={setState}>
      <div className='container mx-auto flex justify-between items-center py-5'>
        <div className='font-bold text-lg'>Rangkuman Simulasi</div>
        <div className='space-x-3'>
          <Link href='/simulasi/lapor'>
            <Button appearance='primary'>+ Simulasi</Button>
          </Link>
        </div>
      </div>

      <div className='container mx-auto'>
        {state.isLoading ? (
          <div className='flex justify-center items-center'>
            <Spinner />
          </div>
        ):(
          <Table>
            <Table.Head>
              <Table.TextHeaderCell>Form</Table.TextHeaderCell>
              <Table.TextHeaderCell>Tahun</Table.TextHeaderCell>
              <Table.TextHeaderCell>Status</Table.TextHeaderCell>
              <Table.TextHeaderCell>Pembetulan Ke</Table.TextHeaderCell>
              <Table.TextHeaderCell>Tanggal Pembuatan</Table.TextHeaderCell>
              <Table.TextHeaderCell>Actions</Table.TextHeaderCell>
            </Table.Head>
            <Table.Body>
              {state.sptSimlations.map((item: any, i: number) => (
                <Table.Row key={i}>
                  <Table.Cell>{item.sptType}</Table.Cell>
                  <Table.Cell>{item.year}</Table.Cell>
                  <Table.Cell>{item.submissionType}</Table.Cell>
                  <Table.Cell>{item.submissionPeriod}</Table.Cell>
                  <Table.Cell>{moment(item.createdAt).format('DD MMM YYYY')}</Table.Cell>
                  <Table.Cell>
                    <Link href={`/simulasi/spt/${item.id}/detil`}><IconButton icon={DocumentOpenIcon} /></Link>
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
        )}
      </div>
    </DashboardLayout>
  )
}

export default Simulasi