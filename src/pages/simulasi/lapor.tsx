import axios from 'axios'
import { Button, DocumentOpenIcon, IconButton, Spinner, Table } from 'evergreen-ui'
import DashboardLayout from 'layouts/DashboardLayout'
import moment from 'moment'
import { NextPage } from 'next'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import { dashboardGetServerSideProps } from 'src/utils/server-side-props'

export const getServerSideProps = dashboardGetServerSideProps

const Simulasi: NextPage = ({ user }: any) => {
  const [state, setState] = useState<any>({
    user,
    isLoading: true,
    sptSimulations: []
  })

  return (
    <DashboardLayout title='Simulasi Lapor Pajak' state={state} setState={setState}>
      <div className='container mx-auto mt-5 space-y-5'>
        <div className='w-full bg-primary-color text-white font-bold p-3'>Pengisian SPT Secara Eletronik (e-filing)</div>
        <div>Untuk pengisian SPT secara elektronik Anda dapat memilih cara berikut:</div>
        <div className='grid grid-cols-1 md:grid-cols-3 gap-5 py-5'>
          <div>
            <div className='w-full bg-primary-color text-white font-bold p-3 mb-5'>Mengunduh Formulir</div>
            <div className='border p-10 text-center font-bold text-gray-500'>
              e-form PDF
            </div>
          </div>
          <div>
            <div className='w-full bg-primary-color text-white font-bold p-3 mb-5'>Mengisi langsung di Situs Web</div>
            <div className='border p-10 text-center font-bold cursor-pointer text-primary-color-hover' onClick={() => window.location.href = '/simulasi/spt'}>
              e-Filing
            </div>
          </div>
          <div>
            <div className='w-full text-white font-bold p-3 mb-5'>.</div>
            <div className='border p-10 text-center font-bold text-gray-500'>
              SPT Masa Pemungut Bea Meterai
            </div>
          </div>
        </div>
      </div>
    </DashboardLayout>
  )
}

export default Simulasi