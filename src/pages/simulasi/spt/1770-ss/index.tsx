import axios from 'axios'
import FormSpt1770SS from 'components/form/FormSpt1770SS'
import FormSptDataFormulir from 'components/form/FormSptDataFormulir'
import DashboardLayout from 'layouts/DashboardLayout'
import { NextPage, NextPageContext } from 'next'
import { useState } from 'react'
import { dashboardGetServerSideProps } from 'src/utils/server-side-props'
import nookies from 'nookies'
import JwtUtil from 'src/utils/jwt'
import { toaster } from 'evergreen-ui'

export const getServerSideProps = dashboardGetServerSideProps

const SPT1770SS: NextPage = ({ user, profile }: any) => {
  const [state, setState] = useState<any>({
    user,
    profile,
    forms: {
      currentIndex: 1,
      dataFormulir: {},
      dataSptDetail: {}
    },
    isLoading: false,
    simulationCode: ''
  })

  const onSubmit = async () => {
    let payload: any = {
      user,
      sptType: '1770SS',
      dataFormulir: state.forms.dataFormulir,
      dataSptDetail: state.forms.dataSptDetail
    }

    if (!payload.dataSptDetail.penghasilanBruto || (
      payload.dataSptDetail.penghasilanBruto && payload.dataSptDetail.penghasilanBruto == ''
    )) return toaster.danger('Error', { description: 'Penghasilan bruto dalam negeri wajib diisi.'})

    if (profile.userType == 'student' && state.simulationCode != '') {
      payload.simulationCode = state.simulationCode
    }

    try {
      setState({ ...state, isLoading: true })
      await axios.post('/api/v1/simulation/spt', payload)
      window.location.href = '/simulasi'
    } catch (e) {
      console.dir(e)
      setState({ ...state, isLoading: false })
    }
  }

  const generateForm = (index: number) => {
    let form: any = null
    switch (index) {
      case 1:
        form = <FormSptDataFormulir state={state} setState={setState} />
        break
      case 2:
        form = <FormSpt1770SS state={state} setState={setState} onSubmit={onSubmit} />
        break
      default:
        form = <FormSptDataFormulir state={state} setState={setState} />
    }

    return form
  }

  return (
    <DashboardLayout title='Simulasi SPT' state={state} setState={setState}>
      <div className='container mx-auto grid grid-cols-1 gap-5 py-5'>
        <div className='w-full bg-primary-color text-white p-3 font-bold'>Formulir SPT 1770 SS</div>
      </div>

      <div className='container mx-auto grid grid-cols-1 gap-5 space-y-10'>
        {generateForm(state.forms.currentIndex)}
      </div>
    </DashboardLayout>
  )
}

export default SPT1770SS