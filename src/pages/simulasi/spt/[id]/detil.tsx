import axios from 'axios'
import PageLoading from 'components/common/PageLoading'
import FormSpt1770SS from 'components/form/FormSpt1770SS'
import DashboardLayout from 'layouts/DashboardLayout'
import { NextPage, NextPageContext } from 'next'
import { useEffect, useState } from 'react'
import { dashboardGetServerSideProps } from 'src/utils/server-side-props'

export const getServerSideProps = dashboardGetServerSideProps

const SptDetail: NextPage = ({ user, query }: any) => {
  const [state, setState] = useState<any>({
    user,
    isLoading: true,
    forms: {
      dataFormulir: {},
      dataSptDetail: {}
    }
  })

  const getSptSimulation = async (id: string) => {
    try {
      const result: any = await axios.get(`/api/v1/simulation/spt/${id}`)

      let spt: any = result.data.spt
      spt.formSpt = JSON.parse(spt.formSpt)

      setState({ ...state, isLoading: false, forms: {
        dataFormulir: spt.formSpt.dataFormulir,
        dataSptDetail: spt.formSpt.dataSptDetail
      } })
    } catch (e: any) {
      setState({ ...state, isLoading: false })
      console.dir(e)
      alert('Terjadi kesalahan.')
    }
  }

  useEffect(() => {
    getSptSimulation(query.id)
  }, [])

  if (state.isLoading) {
    return <PageLoading />
  }

  return (
    <DashboardLayout title='Simulasi SPT' state={state} setState={setState}>
      <div className='container mx-auto'>
        <FormSpt1770SS state={state} setState={setState} />
      </div>
    </DashboardLayout>
  )
}

export default SptDetail