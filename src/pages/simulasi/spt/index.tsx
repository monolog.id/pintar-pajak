import { Button, Label, Pane, Radio } from 'evergreen-ui'
import DashboardLayout from 'layouts/DashboardLayout'
import { NextPage } from 'next'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import { dashboardGetServerSideProps } from 'src/utils/server-side-props'

export const getServerSideProps = dashboardGetServerSideProps

const SimulasiSPT: NextPage = ({ user }: any) => {
  const [state, setState] = useState<any>({
    user,
    answers: {}
  })

  const question1: any = {
    id: '1',
    label: 'Apakah Anda menjalankan Usaha atau Pekerjaan Bebas?',
    options: [
      { label: 'Ya', value: 'yes' },
      { label: 'Tidak', value: 'no' }
    ]
  }

  const question2: any = {
    id: '2',
    label: 'Apakah Anda seorang Suami atau Istri yang menjalankan kewajiban perpajakan terpisah (MT) atau Pisah Harta (PH)?',
    options: [
      { label: 'Ya', value: 'yes' },
      { label: 'Tidak', value: 'no' }
    ]
  }

  const question3: any = {
    id: '3',
    label: 'Apakah penghasilan bruto yang Anda peroleh selama setahun Kurang dari 60 juta Rupiah??',
    options: [
      { label: 'Ya', value: 'yes' },
      { label: 'Tidak', value: 'no' }
    ]
  }

  useEffect(() => {
    console.dir(state)
  }, [state])

  return (
    <DashboardLayout title='Simulasi SPT' state={state} setState={setState}>
      <div className='container mx-auto grid grid-cols-1 gap-5 py-5'>
        <div className='w-full bg-primary-color text-white p-3 font-bold'>Formulir SPT</div>

        <Pane>
          <Label>{question1.label}</Label>
          {question1.options.map((opts: any, i: number) => (
            <Radio key={i} size={16} label={opts.label}
              checked={opts.value == state.answers[question1.id] ? true : false}
              onChange={e => { setState({ ...state, answers: { ...state.answers, [question1.id]: opts.value } }) }} />
          ))}
        </Pane>

        {state.answers[question1.id] && state.answers[question1.id] == 'yes' &&
          <Pane>
            <Label>Anda dapat menggunakan fasilitas upload CSV dari e-SPT</Label>
            <div className='mt-1 italic'>Mohon maaf, saat ini fitur upload CSV dari e-SPT belum tersedia.</div>
          </Pane>
        }

        {state.answers[question1.id] && state.answers[question1.id] == 'no' &&
          <Pane>
            <Label>{question2.label}</Label>
            {question2.options.map((opts: any, i: number) => (
              <Radio key={i} size={16} label={opts.label}
                checked={opts.value == state.answers[question2.id] ? true : false}
                onChange={e => { setState({ ...state, answers: { ...state.answers, [question2.id]: opts.value } }) }} />
            ))}
          </Pane>
        }

        {(state.answers[question1.id] && state.answers[question1.id] == 'no') && (state.answers[question2.id] && state.answers[question2.id] == 'no') &&
          <Pane>
            <Label>{question3.label}</Label>
            {question3.options.map((opts: any, i: number) => (
              <Radio key={i} size={16} label={opts.label}
                checked={opts.value == state.answers[question3.id] ? true : false}
                onChange={e => { setState({ ...state, answers: { ...state.answers, [question3.id]: opts.value } }) }} />
            ))}
          </Pane>
        }

        {(state.answers[question1.id] && state.answers[question1.id] == 'no') &&
         (state.answers[question2.id] && state.answers[question2.id] == 'no') &&
         (state.answers[question3.id] && state.answers[question3.id] == 'yes') &&
          <div><Link href='/simulasi/spt/1770-ss'><Button appearance='primary'>SPT 1770 SS</Button></Link></div>
        }

        {(state.answers[question1.id] && state.answers[question1.id] == 'no') &&
         ((state.answers[question2.id] && state.answers[question2.id] == 'yes') ||
         (state.answers[question2.id] && state.answers[question2.id] == 'no' && state.answers[question3.id] && state.answers[question3.id] == 'no')) &&
          <>
            <Label>Anda Dapat Mengunakan formulir 1770 S, pilihlah form yang akan digunakan </Label>
            <div className='flex justify-start items-center space-x-3'>
              <div><Link href='/simulasi/spt/1770-s'><Button appearance='primary'>Dengan bentuk formulir</Button></Link></div>
              <div><Link href='#'><Button appearance='primary' disabled>Dengan panduan</Button></Link></div>
              <div><Link href='#'><Button appearance='primary' disabled>Dengan upload SPT</Button></Link></div>
            </div>
          </>
        }
      </div>
    </DashboardLayout>
  )
}

export default SimulasiSPT