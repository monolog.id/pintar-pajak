import { NextApiResponse } from 'next'

export const ApiResponseJson = (res: NextApiResponse, body: any, statusCode?: number) => {
  if (body.error)
    return res.status(statusCode || 500).json({ ...body, success: false })

  res.status(statusCode || 200).json({ ...body, success: true })
}