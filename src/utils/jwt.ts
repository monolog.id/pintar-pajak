import jwt from 'jsonwebtoken'

export default class JwtUtil {
  private secret: string

  constructor() {
    this.secret = process.env.NEXTAUTH_SECRET || 'jwtsecret'
  }

  generateToken(data: any) {
    return jwt.sign(data, this.secret)
  }

  verify(token: string) {
    try {
      return jwt.verify(token, this.secret)
    } catch (e) {
      return { error: 'Invalid session.' }
    }
  }
}