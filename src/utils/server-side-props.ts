import prisma from 'lib/prismadb'
import { NextPageContext } from 'next'
import { getSession } from 'next-auth/react'

export const dashboardGetServerSideProps = async (ctx: NextPageContext) => {
  const session: any = await getSession(ctx)
  const redirect: any = {
    destination: '/',
    permanent: false
  }

  if (!session) return { redirect }

  const { user }: any = session
  if (!user) return { redirect }

  let profile: any = await prisma.user.findFirst({ where: { email: user.email }})
  if (profile) {
    delete profile.emailVerified
    delete profile.createdAt
  }

  return {
    props: {
      user,
      profile,
      query: ctx.query
    }
  }
}