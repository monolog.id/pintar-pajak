import { NextPageContext } from 'next'
import nookies from 'nookies'
import JwtUtil from './jwt'

export default function verifyProfile(ctx: NextPageContext) {
  const cookies = nookies.get(ctx)
  const token = cookies['pintarpajak.session']
  if (!token) return {
    error: 'invalid token',
    redirect: {
      destination: '/',
      permanent: false
    }
  }
  let profile: any = new JwtUtil().verify(token)
  if (profile.error) {
    profile.redirect = {
      destination: '/',
      permanent: false
    }
  }
  return profile
}